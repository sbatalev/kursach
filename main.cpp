#include <iostream>
#include <vector>

#include <SDL2/SDL.h>
#include <GLUT/glut.h>

#include "Cube.h"
#include "Peramid.h"

#include "Matrix.h"
#include "Utils.h"
#include "Controller.h"


int bitmap[WIDTH][HEIGHT];

int main() {

    SDL_Renderer *renderer;
    clearBitmap(bitmap);
    auto *ctr = new Controller();

    Cube *cube = new Cube();
    cube->move(100, 150, 0);
    ctr->push(cube);

    auto *p = new Peramid();
    ctr->push(p);
    p->move(400, 150, 0);

    // drawer
    if (SDL_Init(SDL_INIT_VIDEO) == 0) {
        SDL_Window* window = nullptr;
        renderer = nullptr;

        if (SDL_CreateWindowAndRenderer(640, 480, 0, &window, &renderer) == 0) {
            SDL_bool done = SDL_FALSE;

            while (!done) {
                SDL_Event event{};

                SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);
                // clear bitmap
                // drawing figures

                ctr->display(bitmap);

                drawBitmapOnScreen(renderer, bitmap);
                clearBitmap(bitmap);

                SDL_RenderPresent(renderer);
                while (SDL_PollEvent(&event)) {
                    if (event.type == SDL_QUIT) {
                        done = SDL_TRUE;
                    }
                    // event keys here
                    ctr->checkKeys(event);
                }
            }
        }

        if (renderer) {
            SDL_DestroyRenderer(renderer);
        }
        if (window) {
            SDL_DestroyWindow(window);
        }
    }
    SDL_Quit();
    return 0;
}