#include <iostream>
#include <cmath>
#include <SDL2/SDL.h>

#include "Matrix.h"
#include "Utils.h"
#include "Shape.h"
#include "Point.h"

using namespace std;

#ifndef CUBE_H
#define CUBE_H

class Cube : public Shape {
public:
    int premap[WIDTH][HEIGHT];

    const int a = 100;

    double ox = 0;
    double oy = 0;
    double oz = 1;

    Matrix *m;
    Matrix *proj;
    Matrix *copy;

    int ax = 0;
    int ay = 0;
    int az = 0;

    Cube() {
        clearBitmap(premap);

        m = new Matrix();
        m->createM1(8,4);

        proj = new Matrix();
        proj->createM1(8,4);

        copy = new Matrix();

        m->mas[0][0] = 0;
        m->mas[0][1] = 0;
        m->mas[0][2] = 0;

        m->mas[1][0] = 0;
        m->mas[1][1] = a;
        m->mas[1][2] = 0;

        m->mas[2][0] = a;
        m->mas[2][1] = a;
        m->mas[2][2] = 0;

        m->mas[3][0] = a;
        m->mas[3][1] = 0;
        m->mas[3][2] = 0;

        m->mas[4][0] = 0;
        m->mas[4][1] = 0;
        m->mas[4][2] = a;

        m->mas[5][0] = 0;
        m->mas[5][1] = a;
        m->mas[5][2] = a;

        m->mas[6][0] = a;
        m->mas[6][1] = a;
        m->mas[6][2] = a;

        m->mas[7][0] = a;
        m->mas[7][1] = 0;
        m->mas[7][2] = a;

    }
    // draw cube on the screen
    void draw(int (&bitmap)[WIDTH][HEIGHT]) {
        copy->copy(*m);

        moveToZero();
        mtRotate();
        moveAfter();
        projectMatrix();

        // basic operations to draw
        if (isVisible(0, 1, 2, 3)) {
            drawGran(premap, 0, 1, 2, 3); // front
            zakrGran(premap, 2, 0, 1, 2, 3);
            toBitmap(premap, bitmap);
        }

        if (!isVisible(0, 1, 5, 4)) {
            drawGran(premap, 0, 1, 5, 4); // top
            zakrGran(premap, 3, 1, 5, 4, 3);
            toBitmap(premap, bitmap);
        }

        if (!isVisible(1, 2, 6, 5)) {
            drawGran(premap, 1, 2, 6, 5); // right
            zakrGran(premap, 4, 1, 2, 6, 5);
            toBitmap(premap, bitmap);
        }

        if (isVisible(3, 2, 6, 7)) {
            drawGran(premap, 3, 2, 6, 7); // bottom
            zakrGran(premap, 5, 3, 2, 6, 7);
            toBitmap(premap, bitmap);
        }


        if (!isVisible(0, 4, 7, 3)) {
            drawGran(premap, 0, 4, 7, 3); // left
            zakrGran(premap, 6, 0, 4, 7, 3);
            toBitmap(premap, bitmap);
        }

        if (isVisible(7, 6, 5, 4)) {
            drawGran(premap, 7, 6, 5, 4); // back
            zakrGran(premap, 8, 7, 6, 5, 4);
            toBitmap(premap, bitmap);
        }

        m->copy(*copy);

        drawShadow(bitmap);
    }

    bool isVisible(int a, int b, int c, int d) {
        Point n = pointGetNorm(a, b, c);
        Point l = Point(0, 0, -10);
        double result = scalar(n, l);
        if (result > 0) {
            return true;
        }
        return false;
    }

    Point getPoint(int a) {
        Point point(m->mas[a][0], m->mas[a][1], m->mas[a][2]);
        return point;
    }

    Point pointGetNorm(int a, int b, int c) {
        Point n;
        Point p1 = getPoint(a);
        Point p2 = getPoint(b);
        Point p3 = getPoint(c);
        Point v1, v2;
        v1.x = p2.x - p1.x; v1.y = p2.y - p1.y; v1.z = p2.z - p1.z;
        v2.x = p3.x - p1.x; v2.y = p3.y - p1.y; v2.z = p3.z - p1.z;
        n.x = v1.y * v2.z - v1.z * v2.y;
        n.y = v1.z * v2.x - v1.x * v2.z;
        n.z = v1.x * v2.y - v1.y * v2.x;
        return n;
    }

    double scalar(Point &p1, Point &p2) {
        double scalar = p1.x * p2.x + p1.y * p2.y + p1.z * p2.z;
        return scalar;
    }

    void drawShadow(int (&bitmap)[WIDTH][HEIGHT]) {
        int max = int(proj->mas[0][1]);

        for (int i = 0; i < 8; ++i) {
            proj->mas[i][1] < max ? max = proj->mas[i][1] : NULL;
        }
        int dy = abs(GROUND - max);
        for (int i = 0; i < 8; ++i) {
            proj->mas[i][1] += dy;
        }

        drawGran(premap, 0, 1, 2, 3, 7); // front
        zakrGran(premap, 7, 0, 1, 2, 3);
        toBitmap(premap, bitmap);

        drawGran(premap, 0, 1, 5, 4, 7); // top
        zakrGran(premap, 7, 0, 1, 5, 4);
        toBitmap(premap, bitmap);

        drawGran(premap, 1, 2, 6, 5, 7); // right
        zakrGran(premap, 7, 1, 2, 6, 5);
        toBitmap(premap, bitmap);

        drawGran(premap, 3, 2, 6, 7, 7); // bottom
        zakrGran(premap, 7, 3, 2, 6, 7);
        toBitmap(premap, bitmap);

        drawGran(premap, 0, 4, 7, 3, 7); // left
        zakrGran(premap, 7, 0, 4, 7, 3);
        toBitmap(premap, bitmap);

        drawGran(premap, 7, 6, 5, 4, 7); // back
        zakrGran(premap, 7, 7, 6, 5, 4);
        toBitmap(premap, bitmap);
    }

    // recursive zakr
    void zakr(int (&bitmap)[WIDTH][HEIGHT], int x, int y, int color) {
        if ((bitmap[x][y] == 0) && (x >= SPACE) && (x <= SPACE + 640) && (y >= SPACE) && (y <= SPACE + 480)){
            bitmap[x][y] = color;
            zakr(bitmap, x + 1, y, color);
            zakr(bitmap, x - 1, y, color);
            zakr(bitmap, x, y + 1, color);
            zakr(bitmap, x, y - 1, color);
        }
    }

    int minYinProj(int a, int b, int c, int d) {
        int sub1 = proj->mas[a][1] < proj->mas[b][1] ? a : b;
        int sub2 = proj->mas[c][1] < proj->mas[d][1] ? c : d;
        int result = proj->mas[sub1][1] < proj->mas[sub2][1] ? sub1 : sub2;
        return static_cast<int>(proj->mas[result][1]);
    }

    int maxYinProj(int a, int b, int c, int d) {
        int sub1 = proj->mas[a][1] > proj->mas[b][1] ? a : b;
        int sub2 = proj->mas[c][1] > proj->mas[d][1] ? c : d;
        int result = proj->mas[sub1][1] > proj->mas[sub2][1] ? sub1 : sub2;
        return static_cast<int>(proj->mas[result][1]);
    }

    // drawingGran using projection matrix
    void zakrGran(int (&bitmap)[WIDTH][HEIGHT], int color, int a, int b, int c, int d) {
        int min = minYinProj(a, b, c, d) + SPACE;
        int max = maxYinProj(a, b, c, d) + SPACE;
        for (int i = min + 1; i < max; ++i) {
            int counter = 0;
            bool fl = false;
            for (int j = 0; j < WIDTH ; ++j) {
                if ((bitmap[j][i] != 0) && (!fl)) {
                    counter++;
                }
                fl = bitmap[j][i] != 0;
            }
            fl = false;
            if (counter < 2) {
                continue;
            }
            counter = 0;
            for (int j = 0; j < WIDTH ; ++j) {
                if ((bitmap[j][i] != 0) && (!fl)) {
                    counter++;
                }
                fl = bitmap[j][i] != 0;
                if (counter == 1) {
                    bitmap[j][i] = color;
                }
            }
        }
    }


    void drawGran(int (&bitmap)[WIDTH][HEIGHT], int a, int b, int c, int d) {
        drawLine(bitmap, SPACE, proj->mas[a][0], proj->mas[a][1], proj->mas[b][0], proj->mas[b][1]);
        drawLine(bitmap, SPACE, proj->mas[b][0], proj->mas[b][1], proj->mas[c][0], proj->mas[c][1]);
        drawLine(bitmap, SPACE, proj->mas[c][0], proj->mas[c][1], proj->mas[d][0], proj->mas[d][1]);
        drawLine(bitmap, SPACE, proj->mas[d][0], proj->mas[d][1], proj->mas[a][0], proj->mas[a][1]);
    }

    void drawGran(int (&bitmap)[WIDTH][HEIGHT], int a, int b, int c, int d, int color) {
        drawLine(bitmap, SPACE, proj->mas[a][0], proj->mas[a][1], proj->mas[b][0], proj->mas[b][1], color);
        drawLine(bitmap, SPACE, proj->mas[b][0], proj->mas[b][1], proj->mas[c][0], proj->mas[c][1], color);
        drawLine(bitmap, SPACE, proj->mas[c][0], proj->mas[c][1], proj->mas[d][0], proj->mas[d][1], color);
        drawLine(bitmap, SPACE, proj->mas[d][0], proj->mas[d][1], proj->mas[a][0], proj->mas[a][1], color);
    }

    void projectMatrix() {
            auto *prMatrix = new Matrix();
            prMatrix->createProjectionMatrix();
            proj = m->mul(prMatrix);
            normalizeProjection();
    }

    // check keys input
    void checkKeys() {

    }
    void move(double dx, double dy, double dz) {
            auto *moveMatrix = new Matrix();
            moveMatrix->generateMoveMatrix(dx, dy, dz);
            m = m->mul(moveMatrix);
    }

    void scale(double sx, double sy, double sz) {
            moveToZero();
            auto *scaleMatrix = new Matrix();
            scaleMatrix->generateScaleMatrix(sx, sy, sz);
            m = m->mul(scaleMatrix);
            moveAfter();
    }

    void changeAngleX(double angle) {
            ax += angle;
    }

    void changeAngleY(double angle) {
            ay += angle;
    }

    void changeAngleZ(double angle) {
            az += angle;
    }

    void mtRotate() {
        auto zM = new Matrix();
        zM->createRotateMatrixZ(az);
        m = m->mul(zM);
        normalizeCoords();

        auto xM = new Matrix();
        xM->createRotateMatrixX(ax);
        m = m->mul(xM);
        normalizeCoords();

        auto yM = new Matrix();
        yM->createRotateMatrixY(ay);
        m = m->mul(yM);
        normalizeCoords();

    }


private:

    void normalizeCoords() {
            for (int i = 0; i <  m->n; i++) {
                    for (int j = 0; j < 4; j++) {
                            m->mas[i][j] /= m->mas[i][3];
                    }
            }
    }
    void normalizeProjection() {
            for (int i = 0; i <  proj->n; i++) {
                    for (int j = 0; j < 4; j++) {
                            proj->mas[i][j] /= proj->mas[i][3];
                    }
            }
    }
    void moveToZero() {
            ox = (m->mas[0][0] + m->mas[2][0]) /  2;
            oy = (m->mas[0][1] + m->mas[2][1]) /  2;
            oz = (m->mas[0][2] + m->mas[4][2]) /  2 + 1;

            move(-ox, -oy, -oz);
    }
    void moveAfter() {
            move(ox, oy, oz);
    }
};

#endif // CUBE_H
