//
// Created by Сергей Баталев on 04/11/2018.
//

#ifndef KURSACH_POINT_H
#define KURSACH_POINT_H

class Point {
public:
    double x;
    double y;
    double z;

    Point() {}

    Point(int x, int y, int z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }
};

#endif //KURSACH_POINT_H
