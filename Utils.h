//
// Created by Сергей Баталев on 15/10/2018.
//

#ifndef KURSACH_UTILS_H
#define KURSACH_UTILS_H

#include <SDL2/SDL.h>
#include <iostream>

using namespace std;

static const int SPACE = 600;
static const int SPACE2 = SPACE / 2;
static const int WIDTH = 640 + SPACE;
static const int HEIGHT = 480 + SPACE;
static const int GROUND = 300;


void printBitmap(int (&bitmap) [WIDTH][HEIGHT]) {
    for (int i = 0; i < WIDTH; ++i) {
        for (int j = 0; j < HEIGHT; ++j) {
//            cout << bitmap[i][j];
            if (bitmap[i][j] == 1) {
                cout << "yeap";
            }
        }
//        cout << endl;
    }
}

void clearBitmap(int (&bitmap) [WIDTH][HEIGHT]) {
    for (int i = 0; i < WIDTH; ++i) {
        for (int j = 0; j < HEIGHT; ++j) {
            bitmap[i][j] = 0;
        }
    }
}

void toBitmap(int (&pre) [WIDTH][HEIGHT], int (&bitmap) [WIDTH][HEIGHT]) {
    for (int i = SPACE; i < SPACE + 640; i++) {
        for (int j = SPACE; j < SPACE + 480; j++) {
            if (pre[i][j] != 0) {
                bitmap[i][j] = pre[i][j];
            }
        }
    }
    clearBitmap(pre);
}

void drawBitmapOnScreen(SDL_Renderer *renderer,  int ( &bitmap )[WIDTH][HEIGHT]) {
    for (int i = SPACE; i < SPACE + 640; i++) {
        for (int j = SPACE; j < SPACE + 480; j++) {
            if (bitmap[i][j] == 1) {
                SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
                SDL_RenderDrawPoint(renderer, i - SPACE, j - SPACE);
            }
            if (bitmap[i][j] == 2) {
                SDL_SetRenderDrawColor(renderer, 0, 255, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderDrawPoint(renderer, i - SPACE, j - SPACE);
            }
            if (bitmap[i][j] == 3) {
                SDL_SetRenderDrawColor(renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderDrawPoint(renderer, i - SPACE, j - SPACE);
            }
            if (bitmap[i][j] == 4) {
                SDL_SetRenderDrawColor(renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
                SDL_RenderDrawPoint(renderer, i - SPACE, j - SPACE);
            }
            if (bitmap[i][j] == 5) {
                SDL_SetRenderDrawColor(renderer, 255, 0, 255, SDL_ALPHA_OPAQUE);
                SDL_RenderDrawPoint(renderer, i - SPACE, j - SPACE);
            }
            if (bitmap[i][j] == 6) {
                SDL_SetRenderDrawColor(renderer, 60, 60, 60, SDL_ALPHA_OPAQUE);
                SDL_RenderDrawPoint(renderer, i - SPACE, j - SPACE);
            }
            if (bitmap[i][j] == 7) { // shadow color
                SDL_SetRenderDrawColor(renderer, 148, 148, 148, SDL_ALPHA_OPAQUE);
                SDL_RenderDrawPoint(renderer, i - SPACE, j - SPACE);
            }
            if (bitmap[i][j] == 8) {
                SDL_SetRenderDrawColor(renderer, 10, 260, 100, SDL_ALPHA_OPAQUE);
                SDL_RenderDrawPoint(renderer, i - SPACE, j - SPACE);
            }
        }
    }
}

void drawGranOnScreen(SDL_Renderer *renderer,  int ( &bitmap )[WIDTH][HEIGHT]) {
    drawBitmapOnScreen(renderer, bitmap);
    clearBitmap(bitmap);
}

void drawLine(int (&bitmap)[WIDTH][HEIGHT], int SPACE,  int x1, int y1, int x2, int y2) {
    x1 += SPACE;
    x2 += SPACE;
    y1 += SPACE;
    y2 += SPACE;
    double deltaX = abs(x2 - x1);
    double deltaY = abs(y2 - y1);
    double signX = x1 < x2 ? 1 : -1;
    double signY = y1 < y2 ? 1 : -1;

    double error = deltaX - deltaY;

    bitmap[x2][y2] = 1;
    while(x1 != x2 || y1 != y2) {
        bitmap[x1][y1] = 1;
        double error2 = error * 2;

        if(error2 > -deltaY)
        {
            error -= deltaY;
            x1 += signX;
        }
        if(error2 < deltaX)
        {
            error += deltaX;
            y1 += signY;
        }
    }
}

void drawLine(int (&bitmap)[WIDTH][HEIGHT], int SPACE, int x1, int y1, int x2, int y2, int color) {
    x1 += SPACE;
    x2 += SPACE;
    y1 += SPACE;
    y2 += SPACE;
    double deltaX = abs(x2 - x1);
    double deltaY = abs(y2 - y1);
    double signX = x1 < x2 ? 1 : -1;
    double signY = y1 < y2 ? 1 : -1;

    double error = deltaX - deltaY;

    bitmap[x2][y2] = color;
    while(x1 != x2 || y1 != y2) {
        bitmap[x1][y1] = color;
        double error2 = error * 2;

        if(error2 > -deltaY)
        {
            error -= deltaY;
            x1 += signX;
        }
        if(error2 < deltaX)
        {
            error += deltaX;
            y1 += signY;
        }
    }
}


#endif //KURSACH_UTILS_H
