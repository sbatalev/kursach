#include <iostream>
#include <cmath>

using namespace std;

#ifndef MATRIX_CPP
#define MATRIX_CPP

class Matrix {
public:
    double **mas;
    int n;
    int m;


    Matrix() {
        mas = nullptr;
        n = 0;
        m = 0;
    }

    ~Matrix() {
        for (int i = 0; i < n; i++)
            delete mas[i];
        delete[] mas;
    }

    Matrix* copy(Matrix &mt) {
        n = mt.n;
        m = mt.m;
        createM(mt.n, mt.m);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                mas[i][j] = mt.mas[i][j];

         return this;
    }

    Matrix* createM(int n, int m) {
        mas = new double*[n];
        for (int i = 0; i < n; i++) {
            mas[i] = new double[m];
        }
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                mas[i][j] = 0;

        this->m = m;
        this->n = n;
        return this;
    }

    Matrix* createM1(int n, int m) {
        mas = new double*[n];
        for (int i = 0; i < n; i++) {
            mas[i] = new double[m];
        }
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                mas[i][j] = 1;

        this->m = m;
        this->n = n;
        return this;
    }

    Matrix* printMatrix() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                cout << mas[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
        return this;
    }

    Matrix* createSimpleMatrix(int size) {
        this->createM(size, size);
        for (int i = 0; i < size; i++)
            mas[i][i] = 1;
        this->n = size;
        this->m = size;
        return this;
    }

    Matrix* mul(Matrix *mat) {
        if (this->m == mat->n) {
            auto *tmp = new Matrix();
            tmp->createM(this->n, mat->m);
            for (int i = 0; i < this->n; i++) {
                for (int j = 0; j < mat->m; j++) {
                    int rez = 0;
                    for (int r = 0; r < this->m; r++) {
                        rez += this->mas[i][r] * mat->mas[r][j];
                    }
                    tmp->mas[i][j] = rez;
                }
            }
            return tmp;
        }
        return nullptr;
    }
    // for 3d
    Matrix* generateMoveMatrix(double dx, double dy, double dz) {
        createSimpleMatrix(4);
        mas[3][0] = dx;
        mas[3][1] = dy;
        mas[3][2] = dz;
        return this;
    }

    // for 3d
    Matrix* generateScaleMatrix(double sx, double sy, double sz) {
        this->createSimpleMatrix(4);
        this->mas[0][0] = sx;
        this->mas[1][1] = sy;
        this->mas[2][2] = sz;
        return this;
    }


    Matrix* createProjectionMatrix() {
        createSimpleMatrix(4);
        // if need use other projection uncomment here
//        int a = 240, b = -60;
//        mas[0][0] = cos(angleToRad(a));
//        mas[0][1] = cos(angleToRad(b)) * sin(angleToRad(b));
//        mas[1][1] = cos(angleToRad(b));
//        mas[2][0] = sin(angleToRad(a));
//        mas[2][1] = -sin(angleToRad(b)) * cos(a);
//        mas[2][2] = 0;
        return this;
    }






    // for 3d
    Matrix* createRotateMatrixZ(double angle) {
        angle = angleToRad(angle);
        this->createSimpleMatrix(4);
        this->mas[0][0] = cos(angle);
        this->mas[0][1] = sin(angle);
        this->mas[1][0] = -sin(angle);
        this->mas[1][1] = cos(angle);
        return this;
    }

    // for 3d
    Matrix* createRotateMatrixX(double angle) {
        angle = angleToRad(angle);
        this->createSimpleMatrix(4);
        this->mas[1][1] = cos(angle);
        this->mas[1][2] = sin(angle);
        this->mas[2][1] = -sin(angle);
        this->mas[2][2] = cos(angle);
        return this;
    }

    // for 3d
    Matrix* createRotateMatrixY(double angle) {
        angle = angleToRad(angle);
        this->createSimpleMatrix(4);
        this->mas[0][0] = cos(angle);
        this->mas[0][2] = -sin(angle);
        this->mas[2][0] = sin(angle);
        this->mas[2][2] = cos(angle);
        return this;
    }

private:

    double angleToRad(double angle) {
        return angle * M_PI / 180;
    }


};

#endif // MATRIX_CPP