
#include <SDL2/SDL.h>

#include "Utils.h"

#ifndef SHAPE_H
#define SHAPE_H

class Shape {
public:
    virtual void draw(int (&bitmap)[WIDTH][HEIGHT]) = 0;
    virtual void move(double dx, double dy, double dz) = 0;
    virtual void scale(double sx, double sy, double sz) = 0;
    virtual void changeAngleX(double angle) = 0;
    virtual void changeAngleY(double angle) = 0;
    virtual void changeAngleZ(double angle) = 0;
};

#endif // SHAPE_H
